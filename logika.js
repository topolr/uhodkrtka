
let diry; 
let ukazatelSkore = document.querySelector('.score'); 
let ukazatelCasu = document.querySelector('.time-left'); 
let seznamSkore = document.querySelector('.score-list'); 
let krtci;
let krysy; 
let posledniDira; 
let casVyprsel = false; 
let skore = 0; 
let interval; 
let statistikySkore = []; 

// Funkce pro spuštění hry
function startHry() {
  let obtiznost = document.getElementById('difficulty').value; // Výběr obtížnosti
  let minCas, maxCas; // Minimální a maximální čas pro zobrazení krtka
  
  switch (obtiznost) {
    case 'easy':
      minCas = 800;
      maxCas = 1500;
      break;
    case 'medium':
      minCas = 400;
      maxCas = 1000;
      break;
    case 'hard':
      minCas = 200;
      maxCas = 800;
      break;
  }

  ukazatelSkore.textContent = 0; // Reset skóre na začátku hry
  casVyprsel = false; // Reset časovače hry
  skore = 0; // Reset skóre
  aktualizovatCas(20); // Nastavení začátečního času
  vykoukni(minCas, maxCas); // Spustí zobrazování krtků
  
  interval = setInterval(() => {
    if (casVyprsel) {
      clearInterval(interval); // Zastavení časovače, pokud hra skončila
    } else {
      let cas = parseInt(ukazatelCasu.textContent); // Získání zbývajícího času
      if (cas > 0) {
        aktualizovatCas(cas - 1); // Aktualizace zbývajícího času
      } else {
        casVyprsel = true; // Nastavení, že čas vypršel
        clearInterval(interval); // Zastavení časovače
        gameOver(); // Volání funkce pro ukončení hry
      }
    }
  }, 1000); // Aktualizace času každou sekundu
}


/**
 * 
 * @param {*} min 
 * @param {*} max 
 * @returns 
 */

// Funkce generující náhodný čas v zadaném rozmezí
function nahodnyCas(min, max) { 
  return Math.round(Math.random() * (max - min) + min);
}

/**
 * 
 * @param {*} diry 
 * @returns 
 */

// Funkce pro výběr náhodné díry
function nahodnaDira(diry) {
  let idx = Math.floor(Math.random() * diry.length); // Náhodný index díry
  let dira = diry[idx]; // Vybraná díra
  // Zamezení opakování téže díry dvakrát po sobě
  if (dira === posledniDira) {
    return nahodnaDira(diry); // Pokud je stejná díra jako poslední, vybereme znovu
  }
  posledniDira = dira; 
  return dira;
}

/**
 * 
 * @param {*} min 
 * @param {*} max 
 */

// Funkce pro zobrazení krtka
function vykoukni(min, max) {
  let cas = nahodnyCas(min, max); // Náhodný čas pro zobrazení krtka
  let dira = nahodnaDira(diry); // Výběr náhodné díry
  dira.classList.add('up'); // Krtek vyjde nahoru
  setTimeout(() => {
    dira.classList.remove('up'); // Krtek zaleze zpět
    if (!casVyprsel) vykoukni(min, max); // Pokud hra neskončila, zobraz dalšího krtka
  }, cas);
}

/**
 * 
 * @param {*} cas 
 */

// Funkce pro aktualizaci zbývajícího času
function aktualizovatCas(cas) {
  ukazatelCasu.textContent = cas; // Aktualizace ukazatele času
}

/**
 * 
 * @param {*} e 
 */

// Funkce pro zasažení krtka
function prastit(e) {
  skore++; 
  this.parentNode.classList.remove('up'); // Krtek zaleze zpět
  ukazatelSkore.textContent = skore; // Aktualizace skóre na obrazovce
}

// Funkce pro nastavení hry
function nastavHru() {
  let pocetDer = document.getElementById('numberOfHoles').value; // Počet děr zadaný uživatelem
  let hra = document.querySelector('.game'); // Herní pole
  hra.innerHTML = ''; // Vyprázdnit staré díry

  let diryProKrtky = []; 
  let diryProKrysy = []; 

  for (let i = 0; i < pocetDer; i++) {
    let dira = document.createElement('div'); // Vytvoření nové díry
    dira.classList.add('hole'); // Přidání třídy "hole"

    // Rozhodování, zda díra bude obsahovat krtka nebo krysu
    if (Math.random() > 0.5) {
      diryProKrtky.push(i); // Přidání indexu do pole krtků
      let krtek = document.createElement('div'); // Vytvoření nového krtka
      krtek.classList.add('mole'); // Přidání třídy "mole"
      dira.appendChild(krtek); // Přidání krtka do díry
    } else {
      diryProKrysy.push(i); // Přidání indexu do pole krys
    }

    hra.appendChild(dira); // Přidání díry do herního pole
  }

  // Nastavení CSS Grid podle počtu děr
  hra.style.gridTemplateColumns = `repeat(${Math.ceil(Math.sqrt(pocetDer))}, 1fr)`;

  diry = document.querySelectorAll('.hole'); 
  krtci = document.querySelectorAll('.mole'); 

  // Náhodný výběr díry pro krysy, která neobsahuje krtka
  let nahodnaDiraProKrysy = diryProKrysy[Math.floor(Math.random() * diryProKrysy.length)];
  let krysa = document.createElement('div'); 
  krysa.classList.add('rat'); // Přidání třídy "rat"
  diry[nahodnaDiraProKrysy].appendChild(krysa); // Přidání krysy do vybrané díry
  krysy = document.querySelectorAll('.rat'); // Vybrání všech krys

  krtci.forEach(krtek => krtek.addEventListener('click', prastit)); 
  krysy.forEach(krysa => krysa.addEventListener('click', gameOver)); 
}

// Funkce pro aktualizaci zobrazení statistik
function aktualizovatStatistiky() {
  seznamSkore.innerHTML = ''; // Vyprázdnění seznamu statistik
  statistikySkore.forEach((score, index) => {
    let polozka = document.createElement('li'); // Vytvoření nové položky seznamu
    polozka.textContent = `Hra ${index + 1}: ${score} bodů`; // Nastavení textu položky
    seznamSkore.appendChild(polozka); // Přidání položky do seznamu
  });
}

// Funkce pro ukončení hry a zobrazení statistik
function gameOver() {
  alert('Hra skončila!'); 
  casVyprsel = true; 
  statistikySkore.push(skore); // Přidání aktuálního skóre do statistik
  aktualizovatStatistiky(); 
}

// Inicializace hry
nastavHru(); // Volání funkce pro nastavení hry na začátku
